<?php

namespace App\Console\Commands;

use App\Services\NotificationChannelFactory;
use App\Services\Weather\WeatherService;
use App\Services\WeatherProviderFactory;
use Illuminate\Console\Command;

class WeatherCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'weather
                            {provider : The weather provider (available providers: open-weather-map, accu-weather, weatherbit)}
                            {city : The name of the city for which you want to fetch weather data}
                            {channel? : The notification channel (available channels: mail, telegram, console). Default value is console}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Get and notify weather';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle() : int {
        $providerName = $this->argument('provider');
        $city = $this->argument('city');
        $channel = $this->argument('channel') ?? 'console';

        try {
            ($this->getWeatherService($providerName, $channel))->getWeatherAndNotify($providerName, $city);
            $this->info("Weather information for $city sent via $channel.");
        } catch (\Exception $e) {
            $this->error("Error: {$e->getMessage()}");
            return 1;
        }

        return 0;
    }

    protected function getWeatherService($providerName, $channel) {
        return new WeatherService(
            WeatherProviderFactory::createProvider($providerName),
            NotificationChannelFactory::createChannel($channel)
        );
    }
}
