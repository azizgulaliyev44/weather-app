<?php

namespace App\Services\Weather;

use App\Services\Weather\Interfaces\WeatherProviderInterface;
use Illuminate\Support\Facades\Http;

class AccuWeatherProvider implements WeatherProviderInterface
{
    protected $apiKey;

    public function __construct() {
        $this->apiKey = config('services.weather_service.accuweather.api_key');
    }
    public function getWeather(string $city): array {
        // Search for the city location
        $locationResponse = $this->searchCityLocation($city);

        if ($locationResponse->successful()) {
            $locationData = $locationResponse->json();
            if (count($locationData) > 0) {
                $locationKey = $locationData[0]['Key'];

                // Get current conditions for the city
                return $this->getCurrentCondition($locationKey);
            }
        }

        throw new \RuntimeException('Failed to fetch weather data from AccuWeather.');
    }

    protected function searchCityLocation(string $city) {
        return Http::get(config('services.weather_service.accuweather.url_location'), [
            'apikey' => $this->apiKey,
            'q' => $city,
        ]);
    }

    protected function getCurrentCondition($locationKey) : array {
        $conditionsResponse = Http::get(config('services.weather_service.accuweather.url') . "/{$locationKey}", [
            'apikey' => $this->apiKey,
        ]);

        if ($conditionsResponse->successful()) {
            $conditionsData = $conditionsResponse->json();
            if (count($conditionsData) > 0) {
                return $conditionsData[0];
            }
            return [];
        } else {
            throw new \RuntimeException('Failed to fetch weather data from AccuWeather.');
        }
    }
}
