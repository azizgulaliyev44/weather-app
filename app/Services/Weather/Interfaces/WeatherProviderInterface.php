<?php

namespace App\Services\Weather\Interfaces;

interface WeatherProviderInterface
{
    public function getWeather(string $city): array;
}
