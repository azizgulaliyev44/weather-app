<?php

namespace App\Services\Weather;

use App\Services\Weather\Interfaces\WeatherProviderInterface;
use Illuminate\Support\Facades\Http;

class OpenWeatherMapProvider implements WeatherProviderInterface
{
    protected $apiKey;

    public function __construct() {
        $this->apiKey = config('services.weather_service.openweathermap.api_key');
    }
    public function getWeather(string $city): array {
        $response = Http::get(config('services.weather_service.openweathermap.url'), [
            'q' => $city,
            'appid' => $this->apiKey,
            'units' => 'metric'
        ]);

        if ($response->successful()) {
            return $response->json();
        }

        throw new \RuntimeException('Failed to fetch weather data from OpenWeatherMap.');
    }
}
