<?php

namespace App\Services\Weather;

use App\Services\Weather\Interfaces\WeatherProviderInterface;
use Illuminate\Support\Facades\Http;

class DarkSkyProvider implements WeatherProviderInterface
{
    protected $apiKey;

    public function __construct() {
        $this->apiKey = config('services.weather_service.darksky.api_key');
    }

    /**
     * in September 2021, Apple Weather (formerly known as Dark Sky)
     * had discontinued its API service, and access to it was no longer
     * available to new or existing users. Apple had integrated the Dark Sky API
     * into its own services, and it was no longer
     * provided as a standalone service for developers.
     */
    public function getWeather(string $city): array {
//        $latitude = 40.7128;
//        $longitude = -74.0060;
//
//        $response = Http::get(config('services.weather_service.darksky.url') . "/{$this->apiKey}/{$latitude},{$longitude}", [
//            'exclude' => 'minutely,hourly,daily,alerts,flags', // You can customize the data you want
//        ]);
//
//        if ($response->successful()) {
//            return $response->json();
//        }

        throw new \RuntimeException('Failed to fetch weather data from Dark Sky!!! It is deprecated!!!');
    }
}
