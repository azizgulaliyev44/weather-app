<?php

namespace App\Services\Weather;

use App\Services\Notification\Interfaces\NotificationChannelInterface;
use App\Services\NotificationMessageFormatterFactory;
use App\Services\Weather\Interfaces\WeatherProviderInterface;

class WeatherService
{
    public function __construct(
        private WeatherProviderInterface $weatherProvider,
        private NotificationChannelInterface $notificationChannel
    ) {}

    public function getWeatherAndNotify(string $providerName, string $city): void {
        $weatherData = $this->weatherProvider->getWeather($city);
        $this->notificationChannel->sendNotification(
            $this->getMessageText(
                $providerName,
                $city,
                $weatherData
            )
        );
    }

    protected function getMessageText($providerName, $city, $weatherData) : string {
        $messageFormatter = NotificationMessageFormatterFactory::createFormatter($providerName);
        return "Current weather in $city: " . $messageFormatter->getFormattedMessage($weatherData) . ". " .
                "Provider name: " . strtoupper($providerName);
    }
}
