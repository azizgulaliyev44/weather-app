<?php

namespace App\Services\Weather;

use App\Services\Weather\Interfaces\WeatherProviderInterface;
use Illuminate\Support\Facades\Http;

class WeatherbitProvider implements WeatherProviderInterface
{
    protected $apiKey;

    public function __construct() {
        $this->apiKey = config('services.weather_service.weatherbit.api_key');
    }
    public function getWeather(string $city): array {
        $response = Http::get(config('services.weather_service.weatherbit.url'), [
            'city' => $city,
            'key' => $this->apiKey,
        ]);

        if ($response->successful()) {
            return $response->json();
        }

        throw new \Exception("Weatherbit API request failed with status code: {$response->status()}");
    }
}
