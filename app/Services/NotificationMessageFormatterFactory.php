<?php

namespace App\Services;

use App\Services\MessageFormat\AccuWeatherNotificationMessageFormatter;
use App\Services\MessageFormat\DarkSkyNotificationMessageFormatter;
use App\Services\MessageFormat\Interfaces\NotificationMessageFormatterInterface;
use App\Services\MessageFormat\OpenWeatherMapNotificationMessageFormatter;
use App\Services\MessageFormat\WeatherbitNotificationMessageFormatter;

class NotificationMessageFormatterFactory
{
    public static function createFormatter(string $providerName): NotificationMessageFormatterInterface {
        return match ($providerName) {
            'open-weather-map' => new OpenWeatherMapNotificationMessageFormatter(),
            'accu-weather' => new AccuWeatherNotificationMessageFormatter(),
            // Dark Sky weather service is deprecated!!! Not working since 2021!!!
            'dark-sky' => new DarkSkyNotificationMessageFormatter(),
            'weatherbit' => new WeatherbitNotificationMessageFormatter()
        };
    }
}
