<?php

namespace App\Services;

use App\Services\Weather\AccuWeatherProvider;
use App\Services\Weather\DarkSkyProvider;
use App\Services\Weather\Interfaces\WeatherProviderInterface;
use App\Services\Weather\OpenWeatherMapProvider;
use App\Services\Weather\WeatherbitProvider;

class WeatherProviderFactory
{
    public static function createProvider(string $providerName): WeatherProviderInterface {
        switch ($providerName) {
            case 'open-weather-map':
                return new OpenWeatherMapProvider();
            case 'accu-weather':
                return new AccuWeatherProvider();

            /**
             * in September 2021, Apple Weather (formerly known as Dark Sky)
             * had discontinued its API service, and access to it was no longer
             * available to new or existing users. Apple had integrated the Dark Sky API
             * into its own services, and it was no longer
             * provided as a standalone service for developers.
             */
            case 'dark-sky':
                return new DarkSkyProvider();
            case 'weatherbit':
                return new WeatherbitProvider();
            default:
                throw new \InvalidArgumentException("Invalid weather provider: $providerName");
        }
    }
}
