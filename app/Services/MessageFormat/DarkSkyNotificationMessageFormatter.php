<?php

namespace App\Services\MessageFormat;

use App\Services\MessageFormat\Interfaces\NotificationMessageFormatterInterface;

class DarkSkyNotificationMessageFormatter implements NotificationMessageFormatterInterface
{
    public function getFormattedMessage($weatherData): string {
        return $weatherData;
    }
}
