<?php

namespace App\Services\MessageFormat\Interfaces;

interface NotificationMessageFormatterInterface
{
    public function getFormattedMessage($weatherData): string;
}
