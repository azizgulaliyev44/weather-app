<?php

namespace App\Services\MessageFormat;

use App\Services\MessageFormat\Interfaces\NotificationMessageFormatterInterface;

class OpenWeatherMapNotificationMessageFormatter implements NotificationMessageFormatterInterface
{
    public function getFormattedMessage($weatherData): string {
        return "
            Description: {$weatherData['weather'][0]['description']},
            Temperature: {$weatherData['main']['temp']},
            Feel like temp: {$weatherData['main']['feels_like']},
            Wind speed: {$weatherData['wind']['speed']},
            Wind degree: {$weatherData['wind']['deg']}
            ";
    }
}
