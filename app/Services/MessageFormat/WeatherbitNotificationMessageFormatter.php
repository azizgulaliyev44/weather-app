<?php

namespace App\Services\MessageFormat;

use App\Services\MessageFormat\Interfaces\NotificationMessageFormatterInterface;

class WeatherbitNotificationMessageFormatter implements NotificationMessageFormatterInterface
{
    public function getFormattedMessage($weatherData): string {
        return "
            Description: {$weatherData['data'][0]['weather']['description']},
            Temperature: {$weatherData['data'][0]['temp']},
            AQI: {$weatherData['data'][0]['aqi']},
            Sunrise: {$weatherData['data'][0]['sunrise']},
            Sunset: {$weatherData['data'][0]['sunset']}
        ";
    }
}
