<?php

namespace App\Services\MessageFormat;

use App\Services\MessageFormat\Interfaces\NotificationMessageFormatterInterface;

class AccuWeatherNotificationMessageFormatter implements NotificationMessageFormatterInterface
{
    public function getFormattedMessage($weatherData): string {
        return "
            Description: {$weatherData['WeatherText']},
            Temperature: {$weatherData['Temperature']['Metric']['Value']},
            Temperature Unit: {$weatherData['Temperature']['Metric']['Unit']}
        ";
    }
}
