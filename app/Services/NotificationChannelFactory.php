<?php

namespace App\Services;

use App\Services\Notification\ConsoleNotificationChannel;
use App\Services\Notification\Interfaces\NotificationChannelInterface;
use App\Services\Notification\MailNotificationChannel;
use App\Services\Notification\TelegramNotificationChannel;

class NotificationChannelFactory
{
    public static function createChannel(string $channel): NotificationChannelInterface {
        $parts = explode(':', $channel);
        $channelName = $parts[0];

        switch ($channelName) {
            case 'mail':
                return new MailNotificationChannel($parts[1] ?? '');
            case 'telegram':
                return new TelegramNotificationChannel($parts[1] ?? '');
            case 'console':
                return new ConsoleNotificationChannel();
            default:
                throw new \InvalidArgumentException("Invalid notification channel: $channel");
        }
    }
}
