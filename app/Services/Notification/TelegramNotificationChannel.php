<?php

namespace App\Services\Notification;

use App\Services\Notification\Interfaces\NotificationChannelInterface;
use Illuminate\Support\Facades\Http;

class TelegramNotificationChannel implements NotificationChannelInterface
{
    protected $botToken;
    protected $chatId;

    public function __construct(string $chatId = '') {
        $this->botToken = config('telegram.bot.api_token');
        $this->chatId = $chatId ? $chatId : config('telegram.bot.chat_id');
    }
    public function sendNotification(string $message) {
        $response = Http::post("https://api.telegram.org/bot{$this->botToken}/sendMessage", [
            'chat_id' => $this->chatId,
            'text' => $message,
        ]);

        if (!$response->successful()) {
            throw new \Exception("Failed to send Telegram notification. Response code: {$response->status()}");
        }
    }
}
