<?php

namespace App\Services\Notification\Interfaces;

interface NotificationChannelInterface
{
    public function sendNotification(string $message);
}
