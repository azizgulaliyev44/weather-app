<?php

namespace App\Services\Notification;

use App\Mail\WeatherNotificationEmail;
use App\Services\Notification\Interfaces\NotificationChannelInterface;
use Illuminate\Support\Facades\Mail;

class MailNotificationChannel implements NotificationChannelInterface
{
    public function __construct(protected string $recipientEmail) {}
    public function sendNotification(string $message) {
        if (!$this->recipientEmail) {
            throw new \InvalidArgumentException("Recipient email is required for email notifications.");
        }

        $content = [
            'subject' => 'Weather Notification',
            'body' => $message
        ];
        Mail::to($this->recipientEmail)->send(new WeatherNotificationEmail($content));

        echo "Weather notification is successfully send to email!" . PHP_EOL;
    }
}
