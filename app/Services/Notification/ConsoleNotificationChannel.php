<?php

namespace App\Services\Notification;

use App\Services\Notification\Interfaces\NotificationChannelInterface;

class ConsoleNotificationChannel implements NotificationChannelInterface
{
    public function sendNotification(string $message) {
        echo $message . PHP_EOL;
    }
}
