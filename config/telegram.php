<?php

return [
    'bot' => [
        'api_token' => env('TELEGRAM_BOT_TOKEN'),
        'chat_id' => env('TELEGRAM_CHANNEL_CHAT_ID')
    ],
];
