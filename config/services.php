<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Mailgun, Postmark, AWS and more. This file provides the de facto
    | location for this type of information, allowing packages to have
    | a conventional file to locate the various service credentials.
    |
    */

    'mailgun' => [
        'domain' => env('MAILGUN_DOMAIN'),
        'secret' => env('MAILGUN_SECRET'),
        'endpoint' => env('MAILGUN_ENDPOINT', 'api.mailgun.net'),
    ],

    'postmark' => [
        'token' => env('POSTMARK_TOKEN'),
    ],

    'ses' => [
        'key' => env('AWS_ACCESS_KEY_ID'),
        'secret' => env('AWS_SECRET_ACCESS_KEY'),
        'region' => env('AWS_DEFAULT_REGION', 'us-east-1'),
    ],

    'weather_service' => [
        'openweathermap' => [
            'url' => env('OPENWEATHERMAP_URL'),
            'api_key' => env('OPENWEATHERMAP_API_KEY'),
        ],

        'accuweather' => [
            'url' => env('ACCUWEATHER_URL'),
            'url_location' => env('ACCUWEATHER_URL_LOCATION'),
            'api_key' => env('ACCUWEATHER_API_KEY'),
        ],

        'darksky' => [
            'url' => env('DARKSKY_URL'),
            'api_key' => env('DARKSKY_API_KEY'),
        ],

        'weatherbit' => [
            'url' => env('WEATHERBIT_URL'),
            'api_key' => env('WEATHERBIT_API_KEY'),
        ],
    ],

];
