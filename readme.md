## Installation

 ##### Clone project
    git clone https://gitlab.com/azizgulaliyev44/weather-app.git

 ##### Go to the folder application using cd command on your cmd or terminal
    cd weather-app

##### Copy .env.example file to .env
    cp .env.example .env
 
##### Run your cmd or terminal
    composer install

##### Generate app key
     php artisan key:generate

##### After creating .env and running composer install command:
    php artisan optimize:clear

##### Available artisan commands examples
    php artisan weather open-weather-map Tashkent
    php artisan weather accu-weather Tashkent telegram:-1001785971527
    php artisan weather weatherbit Tashkent mail:azizgulaliyev44@gmail.com

   
## Settings env
##### Mail configs. The default working sender Gmail address: weatherappexample@gmail.com, App password: wflqqfutvgfvmerg. You can set own sender gmail configs! 
    MAIL_MAILER=smtp
    MAIL_HOST=smtp.gmail.com
    MAIL_PORT=587
    MAIL_USERNAME=weatherappexample@gmail.com
    MAIL_PASSWORD=wflqqfutvgfvmerg
    MAIL_ENCRYPTION=tls
    MAIL_FROM_ADDRESS=weatherappexample@gmail.com
    MAIL_FROM_NAME="${APP_NAME}"

##### Telegram bot and channel configs. 
##### It sends message to the telegram channel named @weather_notifier. 
##### chat_id of this channel:-1001785971527. You can override these configs.
    TELEGRAM_BOT_TOKEN=6286473135:AAEoMH774hY49_Idt0Gz9VG2MqVz_J2LMm0
    TELEGRAM_CHANNEL_CHAT_ID=-1001785971527

##### Open Weather Map provider config
    OPENWEATHERMAP_API_KEY=3f7fc5bfc85c87aaee15648bc9ec717b
    OPENWEATHERMAP_URL=https://api.openweathermap.org/data/2.5/weather

##### Accu Weather provider config
    ACCUWEATHER_API_KEY=coqGkDWEx1fOGxRhpKvKF56iXCxoBdVB
    ACCUWEATHER_URL_LOCATION=https://dataservice.accuweather.com/locations/v1/cities/search
    ACCUWEATHER_URL=https://dataservice.accuweather.com/currentconditions/v1

##### Weatherbit provider config
    WEATHERBIT_API_KEY=03621714a6d043118e500b5334a819e4
    WEATHERBIT_URL=https://api.weatherbit.io/v2.0/current


